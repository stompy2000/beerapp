//
//  BeerAPI.swift
//  BeerFinder
//

import Foundation

protocol PubsAPIDelegate: class {
    func didFetchData(with model: PubModel)
    func didReceiveError(error: Error)
}

class PubsAPI {
    
    private let dataFetcher: DataFetcher
    weak var delegate: PubsAPIDelegate?
    
    init(dataFetcher: DataFetcher) {
        self.dataFetcher = dataFetcher
    }
    
    func fetchBeers(longitude: Double = 0.141499, lattitude: Double = 51.496466, searchDistance: Int) {
        
        guard let url = makeRequestUrl(long: longitude,
                                       lat: lattitude,
                                       searchDistance: searchDistance) else { return }
        
        dataFetcher.fetchData(from: url) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let data):
                do {
                    let model = try JSONDecoder().decode(PubModel.self, from: data)
                    self.delegate?.didFetchData(with: model)
                }
                catch(let error) {
                    self.delegate?.didReceiveError(error: error)
                }
            case .failure(let error):
                //TODO: Handle error
                print(error)
            }
        }
    }
}

// MARK: - Private
private extension PubsAPI {
    func makeRequestUrl(long: Double, lat: Double, searchDistance: Int) -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "pubcrawlapi.appspot.com"
        urlComponents.path = "/pubcache/"
        urlComponents.queryItems = [URLQueryItem(name: "uId", value: "mike"),
                                    URLQueryItem(name: "lng", value: "\(long)"),
                                    URLQueryItem(name: "lat", value: "\(lat)"),
                                    URLQueryItem(name: "deg", value: "0.00\(searchDistance)")]
        return urlComponents.url
    }
}
