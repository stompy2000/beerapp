//
//  PubLocationModel.swift
//  BeerFinder
//

struct PubLocationModel: Equatable {
    let name: String
    let longitude: Double
    let lattitude: Double
}


