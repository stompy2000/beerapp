//
//  PubModel.swift
//  BeerFinder
//

struct PubModel: Codable {
    let pubs: [PubInfoModel]
    
    enum CodingKeys: String, CodingKey {
        case pubs = "Pubs"
    }
}

