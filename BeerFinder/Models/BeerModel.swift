//
//  BeerModel.swift
//  BeerFinder
//

struct BeerModel {
    let name: String
    let isRegularBeer: Bool
    let pubName: String
    let longitude: Double
    let lattitude: Double
}
