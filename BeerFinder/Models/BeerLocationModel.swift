//
//  BeerLocationModel.swift
//  BeerFinder
//

struct BeerLocationModel {
    let beerName: String
    let pubLocationModels: [PubLocationModel]
}
