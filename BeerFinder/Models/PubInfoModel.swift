//
//  PubInfoModel.swift
//  BeerFinder
//

struct PubInfoModel: Codable {
    let name: String
    let longitude: Double
    let lattitude: Double
    let regularBeers: [String]?
    let guestBeers: [String]?
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case longitude = "Lng"
        case lattitude = "Lat"
        case regularBeers = "RegularBeers"
        case guestBeers = "GuestBeers"
    }
}

