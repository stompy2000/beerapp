//
//  AppDelegate.swift
//  BeerFinder
//
//  Created by Mark Waters on 30/08/2019.
//  Copyright © 2019 John Lewis plc. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    public private(set) var pubsAPI: PubsAPI?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // MARK: - PubsAPI
        let dataFetcher = DataFetcher()
        self.pubsAPI = PubsAPI(dataFetcher: dataFetcher)
        
        return true
    }
}

