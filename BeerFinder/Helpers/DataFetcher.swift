//
//  NetworkHelper.swift
//  BeerFinder
//

import Foundation

class DataFetcher {
    
    private var dataTask: URLSessionDataTask?
    
    func fetchData(from url: URL, with configuration: URLSessionConfiguration? = nil, completionHandler: @escaping (Result<Data, Error>) -> Void) {
        
        let urlConfiguration = configuration ?? URLSessionConfiguration.default
        let session = URLSession(configuration: urlConfiguration)
        let request = URLRequest(url: url)
        
        if let dataTask = dataTask {
            dataTask.cancel()
        }
        
        dataTask = session.dataTask(with: request) {(data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completionHandler(.failure(error))
                }
                guard let data = data else {
                    return
                }
                completionHandler(.success(data))
            }
        }
        
        if let dataTask = dataTask {
            dataTask.resume()
        }
    }
}
