//
//  LocationManager.swift
//  BeerFinder
//

import Foundation
import CoreLocation

protocol LocationProviderDelegate: class {
    func didReceiveLocationUpdate(with location: CLLocationCoordinate2D)
    func didReceiveLocationError(error: Error)
}

enum LocationError: Error {
    case failedToGetLocation
}

class LocationProvider: NSObject {
    
    weak var delegate: LocationProviderDelegate?
    private lazy var locationManager = CLLocationManager()
    private lazy var geocoder = CLGeocoder()
    
    func updateLocationWithGPS() {
        
        configureLocationManager()
        
        if CLLocationManager.locationServicesEnabled() {
            
            let status = CLLocationManager.authorizationStatus()
            
            switch status {
            case .notDetermined, .restricted, .denied:
                locationManager.requestWhenInUseAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.requestLocation()
            @unknown default:
                fatalError()
            }
        } else {
            locationManager(locationManager, didFailWithError: LocationError.failedToGetLocation)
        }
    }
    
    private func configureLocationManager() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
    }
}

extension LocationProvider: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let latitude = locations.last?.coordinate.latitude, let longitude = locations.last?.coordinate.longitude {
            let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            delegate?.didReceiveLocationUpdate(with: location)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate?.didReceiveLocationError(error: error)
    }
}



