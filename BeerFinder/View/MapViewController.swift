//
//  MapViewController.swift
//  BeerFinder
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
    
    var beerLocations: [BeerLocationModel]?
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        tableView.dataSource = self
        tableView.delegate = self
    }
}

// MARK: - TableView Datasource

extension MapViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beerLocations?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Beers"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BeerCell"),
            let beerLocations = self.beerLocations {
            
            cell.textLabel?.text = beerLocations[indexPath.row].beerName.replacingOccurrences(of: "#039;", with: "'")
            let pubModels = beerLocations[indexPath.row].pubLocationModels
            let subTitle = pubModels.map({$0.name}).joined(separator: ", ")
            cell.detailTextLabel?.text = subTitle
            
            return cell
        }
        return UITableViewCell()
    }
}

// MARK: - TableView Delegate

extension MapViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let locations = beerLocations?[indexPath.row] {
            mapView.removeAnnotations(mapView.annotations)
            let points = makeMapViewAnnotations(location: locations)
            mapView.addAnnotations(points)
            mapView.showAnnotations(points, animated: true)
        }
    }
}

// MARK: - Private

private extension MapViewController {
    func makeMapViewAnnotations(location: BeerLocationModel) -> [MKPointAnnotation] {
        let annotations: [MKPointAnnotation] = location.pubLocationModels.map { (pubLocationModel) -> MKPointAnnotation in
            let point = MKPointAnnotation()
            let coordinate = CLLocationCoordinate2D(latitude: pubLocationModel.lattitude,
                                                    longitude: pubLocationModel.longitude)
            point.title = pubLocationModel.name
            point.coordinate = coordinate
            return point
        }
        return annotations
    }
}
