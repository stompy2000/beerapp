//
//  ViewController.swift
//  BeerFinder
//

import UIKit
import CoreLocation

class PubsViewController: UIViewController {

    private var pubsAPI: PubsAPI?
    private var searchDistance: Int = 1
    private var locationProvider: LocationProvider?
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var picker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        pubsAPI = appDelegate?.pubsAPI
        pubsAPI?.delegate = self
        
        picker.dataSource = self
        picker.delegate = self
        
        locationProvider = LocationProvider()
        locationProvider?.delegate = self
        
        imageView.layer.cornerRadius = 10
    }
    
    @IBAction func didTapFetchPubsButton(_ sender: UIButton) {
        locationProvider?.updateLocationWithGPS()
    }
}

// MARK: - Pubs API

extension PubsViewController: PubsAPIDelegate {
    func didFetchData(with model: PubModel) {
        let beers = obtainListOfBeers(from: model)
        let beerDictionary = makeBeerDictionary(with: beers)
        let beerLocationModels: [BeerLocationModel] = beerDictionary.map { (key, value) in
            return BeerLocationModel(beerName: key, pubLocationModels: value)
        }
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        controller.beerLocations = beerLocationModels.sorted(by: { $0.beerName < $1.beerName })
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func didReceiveError(error: Error) {
        // TODO: Add error handling
    }
}

// MARK: - Picker Datasource

extension PubsViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 10
    }
}

// MARK: - Picker Delegate

extension PubsViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch row {
        case 0:
            return "1"
        case 1:
            return "2"
        case 2:
            return "3"
        case 3:
            return "4"
        case 4:
            return "5"
        case 5:
            return "6"
        case 6:
            return "7"
        case 7:
            return "8"
        case 8:
            return "9"
        case 9:
            return "10"
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.searchDistance = row + 1
    }
}

// MARK: - Location

extension PubsViewController: LocationProviderDelegate {
    func didReceiveLocationUpdate(with location: CLLocationCoordinate2D) {
        pubsAPI?.fetchBeers(longitude: location.longitude,
                            lattitude: location.latitude,
                            searchDistance: searchDistance)
    }
    
    func didReceiveLocationError(error: Error) {
        let alertAction = UIAlertAction(title: "OK", style: .default)
        let alertController = UIAlertController(title: "Location Access Disabled",
                                                message: "Please enable location access",
                                                preferredStyle: .alert)
        alertController.addAction(alertAction)
        present(alertController, animated: true)
    }
}

// MARK: - Private

private extension PubsViewController {
    
    func makeBeerDictionary(with models: [BeerModel]) -> [String: [PubLocationModel]] {
        var locationModels: [String: [PubLocationModel]] = [:]
        models.forEach { (model) in
            let pubLocationModel = PubLocationModel(name: model.pubName,
                                                 longitude: model.longitude,
                                                 lattitude: model.lattitude)
            if !locationModels.keys.contains(model.name) {
                locationModels[model.name] = []
            }

            if !(locationModels[model.name]?.contains(pubLocationModel))! {
                locationModels[model.name]?.append(pubLocationModel)
            }
        }
        return locationModels
    }
    
    func obtainListOfBeers(from model: PubModel) -> [BeerModel] {
        let beerModels = model.pubs.flatMap({ (pubInfoModel) in
            extractBeers(from: pubInfoModel)
        })
        return beerModels
    }
    
    func extractBeers(from model: PubInfoModel) -> [BeerModel] {
        let regularBeerModels = model.regularBeers?.map({ BeerModel(name: $0,
                                                                    isRegularBeer: true,
                                                                    pubName: model.name,
                                                                    longitude: model.longitude,
                                                                    lattitude: model.lattitude) }) ?? []
        let guestBeerModels = model.guestBeers?.map({ BeerModel(name: $0,
                                                                isRegularBeer: false,
                                                                pubName: model.name,
                                                                longitude: model.longitude,
                                                                lattitude: model.lattitude) }) ?? []
        return regularBeerModels + guestBeerModels
    }
}
